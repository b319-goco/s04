function factorial(n) {
	if(typeof n !== 'number') return undefined; // to check value if type is number
	if(n<0) return undefined; // to check if the value is not negative
	if(n===0) return 1;  // to check if the value is 0
	if(n===1) return 1; // to check if the value is 1
	return n * factorial(n-1); // for the rest of the numbers >1
}

module.exports = {
	factorial: factorial
}